from django.urls import path
from . import views

app_name = 'main'

urlpatterns = [
    path('', views.home, name='home'),
    path('aboutme/', views.aboutme, name='aboutme'),
    path('hobbies/', views.hobbies, name='hobbies'),
    path('experiences/', views.experiences, name='experiences'),
    path('contact/', views.contact, name='contact'),
    path('story1/', views.story1, name='story1'),
]